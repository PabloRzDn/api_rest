from django.urls import path

from . import views

urlpatterns=[
    path("",views.index, name="index"),
    path("crear/",views.crear, name="crear"),
    path("buscar/",views.buscar, name="buscar"),
    path("editar/",views.editar, name="editar"),
    path("eliminar/",views.eliminar, name="eliminar")
]