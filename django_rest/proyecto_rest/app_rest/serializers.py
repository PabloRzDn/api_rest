from rest_framework.serializers import ModelSerializer

from .models import Usuario

class UsuarioSerializer(ModelSerializer):
    class Meta:
        model=Usuario
        field=("id","nombre","apellido","email","fecha_nacimiento")

        