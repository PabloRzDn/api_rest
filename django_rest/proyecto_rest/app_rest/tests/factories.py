from factory import  Faker
from factory.django import DjangoModelFactory

from ..models import Usuario



class UsuarioFactory(DjangoModelFactory):
    nombre=Faker("first_name")
    apellido=Faker("last_name")
    email=Faker("email")
    fecha_nacimiento=Faker("date")

    class Meta:
        model=Usuario

