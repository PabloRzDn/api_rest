from django.test import TestCase

#from ..models import Usuario
from .factories import UsuarioFactory

class UsuarioTestCase(TestCase):
    def test_str(self):
        usuario=UsuarioFactory()
        self.assertEqual(str(usuario),usuario.nombre)
