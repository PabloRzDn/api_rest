from django.db import models

import uuid

# Create your models here.
class Usuario(models.Model):

    id=models.UUIDField(primary_key=True,default=uuid.uuid4,editable=False)
    nombre=models.CharField(max_length=30)
    apellido=models.CharField(max_length=30)
    email=models.EmailField(max_length=254)
    fecha_nacimiento=models.DateField()

    def __str__(self):
        return self.nombre